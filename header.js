const myHeader = ["question","type","options","correctoption","answer"];
const sheetName = "Sheet1";
const outputCheckHeader = "time" ;
const outputFileSuffix = "_answers.xlsx";

module.exports = {
    myHeader: myHeader,
    sheetName: sheetName,
    outputCheckHeader: outputCheckHeader,
    outputFileSuffix: outputFileSuffix
};