var Stopwatch = function(elem, options) {
  
    var timer       = createTimer(),
        offset,
        clock,
        interval;
     
    // default options
    options = options || {};
    options.delay = options.delay || 1;
   
    // append elements     
    elem.appendChild(timer);
    
    // initialize
    reset();
    
    // private functions
    function createTimer() {
      return document.createElement("span");
    }
    
    function start() {
      if (!interval) {
        offset   = Date.now();
        interval = setInterval(update, options.delay);
      }
    }
    
    function stop() {
      if (interval) {
        clearInterval(interval);
        interval = null;
      }
    }
    
    function reset() {
      clock = options.startTime * 60 * 1000;
      render();
    }
    
    function update() {
      clock -= delta();
      if(clock<0){
        clock =0;
      }
      render();
    }
    
    function render() {
      timer.innerHTML = Math.round(clock/1000) + " sec"; 
    }
    
    function delta() {
      var now = Date.now(),
          d   = now - offset;
      
      offset = now;
      return d;
    }
    function time() {
      return Math.round(clock/1000);
    }
    
    // public API
    this.start  = start;
    this.stop   = stop;
    this.reset  = reset;
    this.time  = time;
  };
  
module.exports={
    Stopwatch:Stopwatch
}