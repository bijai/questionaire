// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
window.$ = require('jquery');
require('bootstrap');
var electron = require('electron').remote;
var fs = require('fs');
var myConst = require('./header');


(function() {
    var $filePicker = $('#filePicker');
    var $filepath = $('#filePath');

    function handleF(/*e*/) {
        var o = electron.dialog.showOpenDialog({
			title: 'Select a file',
			filters: [{
				name: "Spreadsheets",
				extensions: "xls|xlsx|xlsm|xlsb|xml|xlw|xlc|csv|txt|dif|sylk|slk|prn|ods|fods|uos|dbf|wks|123|wq1|qpw|htm|html".split("|")
			}],
			properties: ['openFile']
        });
        if(o.length > 0) {
            $filepath.html(o[0]);
        }
    }
    $filePicker.click(handleF);
})();

(function() {
    var $start = $('#startTestButton');
    
    var $testTime=$('#testTime');
    var $posMark=$('#posMark');
    var $negMark=$('#negMark');
    var $filePath = $('#filePath');

	function handleS(/*e*/) {
        
        //Validations 
        try{
            var testTime = parseInt($testTime.val());
            var posMark = parseFloat($posMark.val());
            var negMark = parseFloat($negMark.val());
            var filePath = $filePath.text();
            if(isNaN(testTime) || isNaN(posMark) || isNaN(negMark))
            throw "Invalid Values Found";
            if(testTime<1)
            throw "Test Time cannot be less than 1 minute";
            console.log({filePath});
            if(filePath == "")
                throw "Please choose a file";
            if (!fs.existsSync(filePath)) {
                throw "The chosen file does not exist"
            }
        }
        catch(err) {
            var m = electron.dialog.showMessageBox({
                title: 'Error!',
                type: 'error',
                message: err.toString()
            });
            return;
        }

        //Set Global Variables 
        // { quizTime: null,posMark:null,negMark:null,fileName:null}
        
        //console.log(electron.getGlobal('sharedObj'));
        electron.getGlobal('sharedObj').testTime = testTime;  
        electron.getGlobal('sharedObj').posMark = posMark;  
        electron.getGlobal('sharedObj').negMark = negMark;  
        electron.getGlobal('sharedObj').fileName = filePath;  
        //console.log(electron.getGlobal('sharedObj'));

        //Full Screen 
        electron.getCurrentWindow().setFullScreen(true);
        // Load quiz.html

        window.location.href = "quiz.html";
        
	}
	$start.click(handleS);
})();

$(window).on('load',function(){
   
});
