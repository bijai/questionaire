// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
window.$ = require('jquery');
require('bootstrap');
require('bootstrap-select');
var electron = require('electron').remote;
var XLSX = require('xlsx');
var fs = require('fs');
var myConst = require('./header');
var Stopwatch = require('./stopwatch');


var json_sheet;  // hold workbook in json format
var counter = 0; //current row
var outputwb; //hold output workbook
var outputpath; //hold output workbook
var fileOpened = false;
var dTimer;
var inputFilePath;
var posMark;
var negMark;
var totalTime;


const resumeID = 1;
const overwriteID = 0;

function inflateSidebar(totalQuestions){
    var $container = $("#qContainer");
    for(i=1;i<=totalQuestions;i++)
    {
        $container.append('<button class="btn btn-secondary m-1" id="qb'+i+'"value="'+i+'">'+i+'</button>')
    }
    
}

(function() {
    var $container = $('#qContainer');
	function handleS(e) {
        console.log(e.target.innerText);
        var jumpTo = parseInt(e.target.innerText);
        if(jumpTo > json_sheet.length)
            return; // junk value

        var $oldqb = $('#qb'+(counter+1));

        $oldqb.removeClass('active');
        
        counter = jumpTo-1;
        output_row();

    }
    
	$container.click( handleS);
})();

function process_wb(filePath) {
    var wb = XLSX.readFile(filePath);
    counter =0;
    json_sheet= XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]]);
    if(json_sheet && json_sheet.length >0){
        outputwb=XLSX.utils.book_new();
        var ws = XLSX.utils.json_to_sheet([
            {}
          ], {header: myConst.myHeader});
          
        XLSX.utils.book_append_sheet(outputwb, ws, myConst.sheetName);
        fileOpened = true;
    }
    else 
        console.log("Parse Error");
};

function force_save()
{
    var written = false;
    var oPath = outputpath;
    while (!written)
    {
        try {
            XLSX.writeFile(outputwb, oPath);
            written=true;
            
        } catch (error) {
            var m = electron.dialog.showMessageBox({
                title: 'Error on writing file',
                type: 'error',
                message: 'Writing to file failed ! Choose a location to save the output file'
            });
            var o = electron.dialog.showSaveDialog({
                title: 'Choose a custom location to save',
                filters: [{
                    name: "Spreadsheets",
                    extensions: "xls|xlsx|xlsm|xlsb|xml|xlw|xlc|csv|txt|dif|sylk|slk|prn|ods|fods|uos|dbf|wks|123|wq1|qpw|htm|html".split("|")
                }]
            });
            oPath=o;
        }

    }
}

function test_completed()
{
    force_save();
    var m = electron.dialog.showMessageBox({
        title: 'Hurray!',
        type: 'info',
        message: 'You have completed the sheet'
    });
    output_row(true);
    clearGlobals();
    $('#fileModal').modal('show');
}


function inflateMCQ()
{
    var $radioGroup = $('#ansMCQ');
    var choices = json_sheet[counter].options;
    choices = choices.split(",");
    for (i = 0; i < choices.length; i++) {
        $radioGroup.append('<div class="form-check"><input class="form-check-input" type="radio" name="ansRadios" id="ansRadios'+(i+1)+'" value="option'+(i+1)+'"><label class="form-check-label" for="ansRadios'+(i+1)+'">'+choices[i]+'</label></div>');
    }
}

var output_row = (function() {
	return function output_row(clear) {
        var $qNo = $('#qNo');
        var $qDiv = $('#qDiv');
        var $aDiv = $('#aDiv');
        var $qb = $('#qb'+(counter+1));
        
        $qb.addClass('active');

        $qDiv.html(json_sheet[counter].question);
        $qNo.html(counter+1);
        
        switch (json_sheet[counter].type){
            case "input":
                $aDiv.html('<div class="form-group"><label for="ansInput">Answer</label><input type="text" class="form-control" id="ansInput" placeholder="Your Answer here."> </div>')
                break;
            case "text":
                $aDiv.html('<div class="form-group"><label for="ansTextArea">Answer</label><textarea class="form-control" id="ansTextArea" rows="3"></textarea></div>');
                break;
                   
            case "mcq":
                $aDiv.html('<fieldset class="form-group" id="ansMCQ"><legend class="col-form-label">Choose one</legend></fieldset>');
                inflateMCQ();
            break;
            
        }
        
        
        window.scrollTo(0,0);
	};
})();


(function() {
    var $startTest = $('#startTest');
    var $navTitle = $('#NavbarTitle');
    var $name = $('#candidateName');
    function handleF(/*e*/) {

        var suffix = $name.val();
        if(suffix==""){
            console.log("Name Cannot be empty");
            $name.addClass('is-invalid');
            return;
        }
        suffix = suffix.replace(" ","_");
        outputpath=inputFilePath.substring(0,inputFilePath.lastIndexOf(".")) + suffix + ".xlsx";
        $navTitle.html($name.val().split(" ")[0]);
        if (fs.existsSync(outputpath)) {
            outputpath=inputFilePath.substring(0,inputFilePath.lastIndexOf(".")) + "_" + Date.now() + ".xlsx";
        }
        $('#candidateModal').modal('hide');
        inflateSidebar(json_sheet.length);
        output_row();
        dTimer.start();
    
	}
	$startTest.click( handleF);
})();

(function() {
    var $savebtn = $('#savebtn');
    
	function handleS(/*e*/) {
        
              
        json_sheet[counter].evft=$('input[name=evft]:checked').val();
        if (t_evft === 'yes')
            json_sheet[counter].attr="N/A";
        else if(inputDropDown.selectpicker('val').includes('Other')){
            var arr = inputDropDown.selectpicker('val');
            arr.push($('#inputAttrText').val());
            json_sheet[counter].attr=arr.toString();
        }
        else {
            json_sheet[counter].attr=inputDropDown.selectpicker('val').toString();
        }
        json_sheet[counter].uvft=$('input[name=uvft]:checked').val();
        json_sheet[counter].uvftText=$('#uvftText').val();
        
        json_sheet[counter].avft=$('input[name=avft]:checked').val();
        json_sheet[counter].avftText=$('#avftText').val();

        json_sheet[counter].comment=$('#commentText').val();

        json_sheet[counter].time=dTimer.time();

        XLSX.utils.sheet_add_json(outputwb.Sheets[outputwb.SheetNames[0]], [
            json_sheet[counter]
          ], {header: myConst.myHeader, skipHeader: true, origin: -1});

        try {
            XLSX.writeFile(outputwb, outputpath);
            
        } catch (error) {
            var m = electron.dialog.showMessageBox({
                title: 'Error on writing file',
                type: 'warning',
                message: 'Writing to file failed ! It will be retried at next form submission or at end of sheet. If the output file ['+outputpath+'] is opened, please close it now.'
            });
        }

        nextRow();
	}
	//submit.addEventListener('click', handleS, false);
})();
(function() {
    var skip = document.getElementById('skip');
	function handleS(/*e*/) {
        if(!fileOpened){
            electron.dialog.showErrorBox('Error','Please open a sheet first !');
            $('#fileModal').modal('show');
            return;
        }
        dTimer.stop();

        json_sheet[counter].evft="";
        json_sheet[counter].attr="";
        json_sheet[counter].uvft="";
        json_sheet[counter].uvftText="";
        json_sheet[counter].avft="";
        json_sheet[counter].avftText="";
        json_sheet[counter].comment="";
        json_sheet[counter].time=dTimer.time();

        XLSX.utils.sheet_add_json(outputwb.Sheets[outputwb.SheetNames[0]], [
            json_sheet[counter]
          ], {header: myConst.myHeader, skipHeader: true, origin: -1});

        try {
            XLSX.writeFile(outputwb, outputpath);
            
        } catch (error) {
            var m = electron.dialog.showMessageBox({
                title: 'Error on writing file',
                type: 'warning',
                message: 'Writing to file failed ! It will be retried at next form submission or at end of sheet. If the output file ['+outputpath+'] is opened, please close it now.'
            });
        }
        nextRow();
	}
	//skip.addEventListener('click', handleS, false);
})();

function clearGlobals()
{
    fileOpened = false;
}


function nextRow()
{
    counter++;
    if((json_sheet.length)<=counter)
        file_completed();
    else output_row();
}



function loadGlobals()
{
    totalTime  = electron.getGlobal('sharedObj').testTime;
    posMark = electron.getGlobal('sharedObj').posMark ;
    negMark = electron.getGlobal('sharedObj').negMark ;
    inputFilePath = electron.getGlobal('sharedObj').fileName;

    if(totalTime == null || posMark == null || negMark == null || inputFilePath == null)
    {
        // TODO: Fatal Error 
    }
}

function loadSheet()
{
    process_wb(inputFilePath);
    $('#totalQNo').html(json_sheet.length);
    $('#totalTime').html(totalTime + " minutes");

}

$(window).on('load',function(){
    //Load Globals
    loadGlobals();
    //Load Input Excel to variable
    loadSheet();
    //Show Modal for candidate info
    dTimer = new Stopwatch.Stopwatch(document.getElementById('Timer'), {delay: 1000,startTime:totalTime });
    $('#candidateModal').modal('show');
    
});

